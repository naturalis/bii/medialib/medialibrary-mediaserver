-- MariaDB dump 10.19  Distrib 10.5.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: medialib
-- ------------------------------------------------------
-- Server version	10.5.16-MariaDB-1:10.5.16+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `deleted_media`
--

DROP TABLE IF EXISTS `deleted_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deleted_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `regno` varchar(48) NOT NULL,
  `producer` varchar(64) DEFAULT NULL,
  `owner` varchar(64) NOT NULL,
  `source_file` varchar(255) DEFAULT NULL,
  `source_file_size` int(10) unsigned DEFAULT NULL,
  `source_file_created` datetime DEFAULT NULL,
  `source_file_etag` char(32) DEFAULT NULL,
  `source_file_aws_uri` varchar(255) DEFAULT NULL,
  `source_file_backup_created` datetime DEFAULT NULL,
  `master_file` varchar(255) DEFAULT NULL,
  `master_published` datetime DEFAULT NULL,
  `www_dir` varchar(127) DEFAULT NULL,
  `www_file` varchar(127) DEFAULT NULL,
  `www_published` datetime DEFAULT NULL,
  `tar_file_id` int(10) unsigned DEFAULT NULL,
  `backup_group` tinyint(3) unsigned NOT NULL,
  `backup_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `master_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `www_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `date_deleted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `date_deleted` (`date_deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deleted_media`
--

LOCK TABLES `deleted_media` WRITE;
/*!40000 ALTER TABLE `deleted_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `deleted_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `regno` varchar(48) NOT NULL,
  `producer` varchar(64) DEFAULT NULL,
  `owner` varchar(64) NOT NULL,
  `source_file` varchar(255) DEFAULT NULL,
  `source_file_size` int(10) unsigned DEFAULT NULL,
  `source_file_created` datetime DEFAULT NULL,
  `source_file_etag` char(32) DEFAULT NULL,
  `source_file_aws_uri` varchar(255) DEFAULT NULL,
  `source_file_backup_created` datetime DEFAULT NULL,
  `master_file` varchar(255) DEFAULT NULL,
  `master_published` datetime DEFAULT NULL,
  `www_dir` varchar(127) DEFAULT NULL,
  `www_file` varchar(127) DEFAULT NULL,
  `www_published` datetime DEFAULT NULL,
  `tar_file_id` int(10) unsigned DEFAULT NULL,
  `backup_group` tinyint(3) unsigned NOT NULL,
  `backup_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `master_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `www_ok` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_01` (`regno`),
  KEY `tar_file_id` (`tar_file_id`),
  KEY `source_file_created` (`source_file_created`),
  KEY `masters_unprocessed` (`producer`,`master_ok`),
  KEY `www_unprocessed` (`producer`,`master_ok`,`www_ok`),
  KEY `offload_unprocessed` (`backup_group`,`backup_ok`)
) ENGINE=MyISAM AUTO_INCREMENT=8090700 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (4687350,'test','backup','backup','/data/_backup.street/staging/20140203184303/backup/phase2/004687350-test.tif',34110293,'2014-02-03 17:43:03','bbe56932d8b105ab0053cfe7a8272560',NULL,'2018-01-14 03:47:27','/data/masters/backup/20140203/004687350-test.jpg','2014-02-03 17:43:49','/data/www/backup/20140203','004687350-test.jpg','2014-02-03 17:46:27',206785,0,1,1,1);
INSERT INTO `media` VALUES (4687351,'uil','backup','backup','/data/_backup.street/staging/20140203184303/backup/phase2/uil.mp4',3786622,'2022-06-07 15:25:03','1cdd2da02a40589db7877eb25ecd65f3',NULL,'2022-06-07 15:25:03','','2022-06-07 15:25:03','/data/www/backup/20220607','uil.mp4','2022-06-07 15:25:03',206785,0,1,1,1);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `oai_pmh_view_1`
--

DROP TABLE IF EXISTS `oai_pmh_view_1`;
/*!50001 DROP VIEW IF EXISTS `oai_pmh_view_1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `oai_pmh_view_1` (
  `id` tinyint NOT NULL,
  `regno` tinyint NOT NULL,
  `producer` tinyint NOT NULL,
  `www_dir` tinyint NOT NULL,
  `www_file` tinyint NOT NULL,
  `poll_date` tinyint NOT NULL,
  `status` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `oai_pmh_view_2`
--

DROP TABLE IF EXISTS `oai_pmh_view_2`;
/*!50001 DROP VIEW IF EXISTS `oai_pmh_view_2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `oai_pmh_view_2` (
  `id` tinyint NOT NULL,
  `regno` tinyint NOT NULL,
  `producer` tinyint NOT NULL,
  `www_dir` tinyint NOT NULL,
  `www_file` tinyint NOT NULL,
  `poll_date` tinyint NOT NULL,
  `status` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tar_file`
--

DROP TABLE IF EXISTS `tar_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tar_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  `remote_dir` varchar(95) DEFAULT NULL,
  `backup_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tar_file`
--

LOCK TABLES `tar_file` WRITE;
/*!40000 ALTER TABLE `tar_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `tar_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `oai_pmh_view_1`
--

/*!50001 DROP TABLE IF EXISTS `oai_pmh_view_1`*/;
/*!50001 DROP VIEW IF EXISTS `oai_pmh_view_1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `oai_pmh_view_1` AS select `a`.`id` AS `id`,`a`.`regno` AS `regno`,`a`.`producer` AS `producer`,`a`.`www_dir` AS `www_dir`,`a`.`www_file` AS `www_file`,`a`.`www_published` AS `poll_date`,'ACTIVE' AS `status` from (`media` `a` left join `deleted_media` `b` on(`a`.`id` = `b`.`id`)) where (`b`.`id` is null or `b`.`source_file_created` > `b`.`date_deleted`) and `a`.`www_ok` = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `oai_pmh_view_2`
--

/*!50001 DROP TABLE IF EXISTS `oai_pmh_view_2`*/;
/*!50001 DROP VIEW IF EXISTS `oai_pmh_view_2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `oai_pmh_view_2` AS select `a`.`id` AS `id`,`a`.`regno` AS `regno`,`a`.`producer` AS `producer`,`a`.`www_dir` AS `www_dir`,`a`.`www_file` AS `www_file`,`a`.`date_deleted` AS `poll_date`,'DELETED' AS `status` from (`deleted_media` `a` left join `media` `b` on(`a`.`id` = `b`.`id`)) where `b`.`id` is null */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-30 14:47:54
