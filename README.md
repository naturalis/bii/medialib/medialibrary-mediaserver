Medialib - Mediaserver
======================

Medialib Mediaserver is the web application that serves images and other
files stored for many digitized collections at the
[Naturalis Biodiversity Center](https://naturalis.nl).


Get started
-----------

You need a recent version of docker.

- Clone this repository
- Install dependencies:

```console
docker run --rm --interactive --tty \
  --volume $PWD:/var/www \
  registry.gitlab.com/naturalis/bii/medialib/docker-medialib/php-dev:latest /bin/sh -c 'composer install'

```

- Add `medialib.dryrun.link` to your local `/etc/hosts` file:

```
sudo echo "127.0.0.1 medialib.dryrun.link" >> /etc/hosts
```

- Create a `.env` file with two values needed for the ssl certificate:

```
AWS_ACCESS_KEY_ID=<copy paste bitwarden secret traefik-route53-dryrun.link key id>
AWS_SECRET_ACCESS_KEY=<copy paste bitwarden secret traefik-route53-dryrun.link access key>
```

Also add the S3 credentials to serve the images from the S3 bucket:

```
MEDIALIB_S3_REGION=...
MEDIALIB_S3_BUCKET=...
MEDIALIB_S3_ACCESS_KEY_ID=...
MEDIALIB_S3_SECRET_ACCESS_KEY=...
```

You can find the credentials of the development account in the vault,
or you can setup your own s3 bucket if you prefer.

Before starting docker compose. Be sure to check you have no docker environments,
database server or webserver running in your local development environment.

- `docker compose up`, which is equal to:

```
docker compose -f docker-compose.yml -f docker-compose.override.yml up
```

- Open your browser to https://medialib.dryrun.link

You should now see a welcoming form **Welkom bij de NBC Media Library**. The ssl certificate
routine can take some time.

Deployment
----------

The deployment of this project is done by the gitlab ci pipeline which is run
on a worker hosted by Naturalis. The `development` branch is deployed
automatically on acceptance instances, a tagged release will be deployed on
the production site. These machines are initialy setup using
[terraform-medialib-compute](https://gitlab.com/naturalis/bii/medialib/terraform-medialib-compute) and
[ansible-medialib](https://gitlab.com/naturalis/bii/medialib/ansible-medialib).

During the deployment a series of [tasks](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/-/tree/develop/ansible/roles/deploy/tasks) are completed which prepares the environment,
create the [initial database infrastructure](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/-/blob/develop/ansible/roles/deploy/tasks/database_init.yml) and start the
[docker-compose setup](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/-/blob/develop/ansible/roles/deploy/tasks/main.yml). It also sets [timers and services for database backup](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/-/blob/develop/ansible/roles/deploy/tasks/database_backup.yml).

You can test the ansible deployment locally by
[first setting up a vagrant machine](https://gitlab.com/naturalis/bii/medialib/ansible-medialib#local-ansible-testing-and-development)
and then locally run the playbook for deployment:

```
IMAGE_VERSION=ansible-deployment ansible-playbook -i ansible/inventory/hosts.ini ansible/deploy.yml
```

Be sure to check and follow the instructions described in
[MR 12: Local ansible deployment](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/-/merge_requests/12).


Troubleshooting
---------------

To troubleshoot the intermediary docker images or run the exact same setup locally:

```
export DATA_PATH=${PWD}/data
docker compose -f docker-compose.yml up
```

This will try to pull the **latest** build of the docker images and use the data directories
in your local copy of the repository. It does not use the code or the nginx config of your
local copy.

If you want to use the recent build of your current branch (if it is succesfully built):

```
export IMAGE_VERSION=`git symbolic-ref --short HEAD`
docker compose -f docker-compose.yml up
```

You can also choose to specify any **IMAGE_VERSION** as long as it is built in the
docker image repository of this project.

If the docker image does not exist, you will see errors like:

```
manifest for registry.gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver/traefik:latest not found: manifest unknown: manifest unknown
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

```
pre-commit autoupdate
pre-commit install
```

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.
