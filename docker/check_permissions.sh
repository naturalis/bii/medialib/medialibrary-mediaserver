#!/bin/sh
set -eu

if [ -z ${1+x} ]; then
  echo Missing user argument
  exit 1
fi
if [ -z ${2+x} ]; then
  echo Missing image argument
  exit 2
fi
user=$1
image=$2

docker pull "$image" > /dev/null
list=$(docker run --rm "$image" find . -follow '(' -type f -or -type d ')' '(' -user "$user" -or -group "$user" -or -perm /o+w ')')
if [ -n "$list" ]; then
  echo Found files with wrong permissions:
  echo "$list"
  exit 1;
fi
