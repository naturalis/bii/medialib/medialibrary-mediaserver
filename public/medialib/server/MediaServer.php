<?php

namespace nl\naturalis\medialib\server;

use Exception;
use Monolog\Logger;
use nl\naturalis\medialib\server\db\dao\MediaServerDAO;
use nl\naturalis\medialib\util\context\Context;

/**
 * @author ayco_holleman
 */
class MediaServer
{
    /**
     *
     * @var Context
     */
    private $_context;

    /**
     *
     * @var Logger
     */
    private $_logger;

    /**
     *
     * @var MediaServerDAO
     */
    private $_dao;


    public function __construct()
    {
        if (!defined('APPLICATION_PATH')) {
            throw new Exception('APPLICATION_PATH not defined.');
        }
        $this->_context = new Context();
        $this->_context->initializeLogging();
        $this->_logger = $this->_context->getLogger();
        $this->_dao = new MediaServerDAO($this->_context);
    }


    public function handleRequest()
    {
        $debug = getenv('DEBUG') ? : false;
        $this->_context->setProperty('debug', $debug);

        if ($this->filterBadAgents()) {
            header('HTTP/1.0 403 Forbidden');
            return;
        }

        try {
            $path = $_SERVER['REQUEST_URI'];
            $this->_logger->info('Request: ' . $path);
            $dispatcher = new RequestDispatcher($path, '/');
            $controller = $dispatcher->getController();
            if ($controller === null) {
                $this->showWelcomePage();
                return;
            }
            $controller->setContext($this->_context);
            $controller->setRequestDispatcher($dispatcher);
            $method = $dispatcher->getActionMethod();
            $controller->$method();
        } catch (Exception $e) {
            if ($debug) {
                header('Content-Type:text/plain');
                echo "\n" . $e->getTraceAsString();
                echo "\n" . basename($e->getFile()) . ' (' . $e->getLine() . '): ' . $e->getMessage();
            } else {
                header('X-Error-Message: ' . $e->getMessage(), true, 500);
            }
            $this->_logger->debug("\n" . $e->getTraceAsString());
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . ' - ' . basename($e->getFile()) . ' (' . $e->getLine(
                ) . '): ' . $e->getMessage()
            );
            exit();
        }
    }

    private function filterBadAgents()
    {
        $badAgents = getenv('BAD_AGENTS') ? : '';
        $badAgents = explode(';', $badAgents);
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        foreach ($badAgents as $badAgent) {
            if (str_contains($userAgent, $badAgent)) {
                return true;
            }
        }
        return false;
    }


    protected function showWelcomePage()
    {
        $baseUrl = getenv('BASE_URL') ?: '/';
        $_REQUEST['BASE_URL'] = $baseUrl;

        if (isset($_POST) && is_array($_POST) && count($_POST) !== 0) {
            $_REQUEST['RESULT_SET'] = $this->_dao->searchMedia($_POST);
            $_REQUEST['RESULT_SET_SIZE'] = $this->_dao->countMedia($_POST);
        } else {
            $_REQUEST['RESULT_SET'] = null;
            $_REQUEST['RESULT_SET_SIZE'] = 0;
        }
        $_REQUEST['PRODUCER_ARRAY'] = $this->_dao->getProducers();
        include APPLICATION_PATH . DIRECTORY_SEPARATOR . 'welcome.php';
    }
}
