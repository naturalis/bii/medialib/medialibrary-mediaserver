<?php

namespace nl\naturalis\medialib\server\controller;

use Aws\S3\S3Client;
use Exception;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\MimeTypeDetection\ExtensionMimeTypeDetector;
use nl\naturalis\medialib\server\AbstractController;
use nl\naturalis\medialib\util\FileUtil;

/**
 * The FileController is responsible for serving media based on their
 * registration number, which, by design, is equal to their file name
 * minus the file extension.
 *
 * @author ayco_holleman
 */
class FileController extends AbstractController
{
    const REGNO_PARAM = 'id';
    const FORMAT_PARAM = 'format';
    const FORMAT_SMALL = 'small';
    const FORMAT_MEDIUM = 'medium';
    const FORMAT_LARGE = 'large';
    const FORMAT_MASTER = 'master';

    private $isHeadRequest;

    public function __construct()
    {
        $client = new S3Client(
            [
            'credentials' => [
                'key' => getenv('MEDIALIB_S3_ACCESS_KEY_ID') ?: false,
                'secret' => getenv('MEDIALIB_S3_SECRET_ACCESS_KEY') ?: false
            ],
            'region' => getenv('MEDIALIB_S3_REGION') ?: false,
            'version' => getenv('MEDIALIB_S3_VERSION') ?: '2006-03-01'
            ]
        );
        $adapter = new AwsS3V3Adapter($client, getenv('MEDIALIB_S3_BUCKET') ?: false);
        $this->_filesystem = new Filesystem($adapter);
        $this->isHeadRequest = $_SERVER['REQUEST_METHOD'] == 'HEAD';
    }

    public function isSingleActionController()
    {
        return true;
    }


    /**
     * @throws Exception
     * @throws FilesystemException
     */
    public function indexAction()
    {
        $regno = urldecode($this->_dispatcher->getParam(self::REGNO_PARAM));
        if ($regno === null) {
            throw new Exception("Missing parameter: " . self::REGNO_PARAM);
        }
        $format = $this->_dispatcher->getParam(self::FORMAT_PARAM, self::FORMAT_MEDIUM);
        $media = $this->_dao->getMedia($regno);
        if ($media === false) {
            $this->_logger->error($_SERVER['REQUEST_URI'] . " -  resource not found for: $regno");
            header('HTTP/1.0 404 Not Found');
            exit();
        }
        if ((int)$media->www_ok === 0) {
            $this->_logger->error($_SERVER['REQUEST_URI'] . " -  Requested resource not ready to be served yet: $regno");
            header('HTTP/1.0 404 Not Found');
            exit();
        }
        $path = $this->getLocationOnServer($media, $format);

        try {
            $this->_filesystem->fileExists($path);
        } catch (FilesystemException $exception) {
            $this->_logger->error($_SERVER['REQUEST_URI'] . " -  resource not found at expected location: $path");
            header('HTTP/1.0 404 Not Found');
            exit();
        }

        $this->_logger->info("Serving \"$path\"");
        if ($format == self::FORMAT_MASTER) {
            $this->serveDownload($path);
        } elseif ($this->isImage($path)) {
            $this->serveImage($path, $media->www_published, $media->source_file_etag);
        } else {
            $this->serveFile($path);
        }
    }

    private function getLocationOnServer($media, $format)
    {
        if ($this->isImage($media->www_file)) {
            if ($format == self::FORMAT_MASTER) {
                $location = "{$media->master_file}";
            } else {
                $location = "{$media->www_dir}/{$format}/{$media->www_file}";
            }
        } else {
            $location = "{$media->www_dir}/{$media->www_file}";
        }
        return $location;
    }

    private function isImage($path)
    {
        $ext = FileUtil::getExtension($path, true);
        return in_array($ext, ['jpg', 'jpeg', 'png', 'gif']);
    }

    private function serveDownload($path)
    {
        header("X-Sendfile: $path");
        header("Content-type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . basename($path) . '"');

        if ($this->isHeadRequest) {
            exit();
        }

        try {
            $stream = $this->_filesystem->readStream($path);
            fpassthru($stream);
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not send file contents: " . $exception->getMessage()
            );
            header('HTTP/1.0 404 Not Found');
        }
        exit();
    }

    /**
     * @throws FilesystemException
     */
    private function serveImage($path, $published = null, $etag = null)
    {
        try {
            if (!$this->_filesystem->fileExists($path)) {
                header('HTTP/1.0 404 Not Found');
                exit();
            }
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not check if image exists: " . $exception->getMessage()
            );
            exit();
        }

        // For HEAD requests, do not download the file, but return the headers based on the extension.
        // Use publication date in the database for last modified if available.
        $lastModified = $published ? strtotime($published) : $this->_filesystem->lastModified($path);

        $maxage = getenv('MEDIALIB_CACHE_MAX_AGE') ?: (60 * 60 * 24);

        header('Cache-Control: max-age=' . $maxage);
        header("Last-Modified: " . gmdate('D, d M Y H:i:s', $lastModified) . ' UTC');
        header("Etag: $etag");

        if ($this->isHeadRequest) {
            $detector = new ExtensionMimeTypeDetector();
            header('Content-Type: ' . $detector->detectMimeTypeFromPath($path));
            return;
        }

        try {
            header('Content-Type: ' . $this->_filesystem->mimeType($path));
            echo $this->_filesystem->read($path);
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not read image: " . $exception->getMessage()
            );
        }
    }

    private function serveFile($path)
    {
        try {
            if (!$this->_filesystem->fileExists($path)) {
                header('HTTP/1.0 404 Not Found');
                exit();
            }
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not check if file exists: " . $exception->getMessage()
            );
            exit();
        }

        try {
            $fileSize = $this->_filesystem->fileSize($path);
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not retrieve filesize: " . $exception->getMessage()
            );
            header('HTTP/1.0 404 Not Found');
            exit();
        }
        header('Content-Length: ' . $fileSize);

        // For HEAD requests, do not stream the file, but return the header based on the extension
        if ($this->isHeadRequest) {
            $detector = new ExtensionMimeTypeDetector();
            header('Content-Type: ' . $detector->detectMimeTypeFromPath($path));
            exit();
        }

        try {
            header('Content-Type: ' . $this->_filesystem->mimeType($path));
            $stream = $this->_filesystem->readStream($path);
            fpassthru($stream);
        } catch (FilesystemException $exception) {
            $this->_logger->error(
                $_SERVER['REQUEST_URI'] . " - Could not send file contents: " . $exception->getMessage()
            );
            header('HTTP/1.0 404 Not Found');
        }
        exit();
    }
}
