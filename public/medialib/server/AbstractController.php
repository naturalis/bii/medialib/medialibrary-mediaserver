<?php

namespace nl\naturalis\medialib\server;

use nl\naturalis\medialib\server\db\dao\MediaServerDAO;
use nl\naturalis\medialib\util\context\Context;
use Monolog\Logger;

/**
 *
 * @author ayco_holleman
 */
abstract class AbstractController
{
    /**
     *
     * @var Context
     */
    protected $_context;

    /**
     *
     * @var MediaServerDAO
     */
    protected $_dao;

    /**
     * @var $_filesystem
     */
    protected $_filesystem;

    /**
     *
     * @var Logger
     */
    protected $_logger;

    /**
     *
     * @var RequestDispatcher
     */
    protected $_dispatcher;


    public function setContext(Context $context)
    {
        $this->_context = $context;
        $this->_dao = new MediaServerDAO($context);
        $this->_logger = $context->getLogger(get_called_class());
    }


    public function setRequestDispatcher(RequestDispatcher $dispatcher)
    {
        $this->_dispatcher = $dispatcher;
    }


    /**
     * Whether or not this controller consists of just one action. Subclasses
     * of AbstractController can override this method by returning true, and
     * thus declare themselves to be a single action controller. For a single
     * action controller the following applies:
     * <ul>
     * <li>
     * The URL for this action is assumed NOT to have a path segment for the
     * action; the request dispatching mechanism automatically dispatches to
     * the one and only action in the controller. In other words, the next
     * path segment after the controller segment is assumed to be the name of
     * the first path parameter.
     * </li>
     * <li>
     * The method implementing the one and only action is assumed to be
     * indexAction(). The request dispatching mechanism will always call this
     * method for single action controllers.
     * </li>
     * </ul>
     * Subclasses of AbstractController can override this method by returning
     * true, and thus declare themselves to be a single action controller.
     *
     * @return boolean
     */
    public function isSingleActionController()
    {
        return false;
    }
}
