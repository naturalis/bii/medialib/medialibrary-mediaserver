<?php

namespace nl\naturalis\medialib\util\context;

use Exception;
use Monolog\Logger;
use Monolog\Level;
use Monolog\Handler\StreamHandler;
use PDO;
use PDOException;

/**
 * A {@code Context} object sets up and provides shared services (logging) and
 * resources (pdo) to the classes that participating in a medialib process.
 *
 * @author ayco_holleman
 */
class Context
{
    /**
     *
     * @var Logger
     */
    private static $_logger;
    /**
     *
     */
    private $_logHandler;
    /**
     *
     * @var PDO
     */
    private $_pdo;
    /**
     *
     * @var array
     */
    private $_props;


    public function __construct()
    {
    }


    /**
     *
     * @return Logger
     */
    public function getLogger()
    {
        if (self::$_logger === null) {
            throw new Exception('Logging not initialized yet. Call Context::initializeLogging() first');
        }
        return self::$_logger;
    }


    public function getLogFile()
    {
    }


    /**
     *
     * @return PDO
     */
    public function getSharedPDO()
    {
        if ($this->_pdo === null) {
            $host = getenv('MYSQL_HOST') ?: 'database';
            $user = getenv('MYSQL_USER') ?: 'medialib';
            $password = getenv('MYSQL_PASSWORD') ?: 'medialib_password';
            $db = getenv('MYSQL_DATABASE') ?: 'medialib';
            $dsn = "mysql:host={$host};dbname={$db}";
            try {
                $this->_pdo = new PDO(
                    $dsn,
                    $user,
                    $password,
                    array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                    )
                );
            } catch (PDOException $e) {
                self::$_logger->error('Could not connect to database - ' . $e->getMessage());
                throw $e;
            }
        }
        return $this->_pdo;
    }


    public function shutdown()
    {
        if ($this->_logHandler !== null) {
            $this->_logHandler->close();
            $this->_logHandler = null;
        }
        $this->_pdo = null;
    }



    public function setProperty($name, $val)
    {
        $this->_props[$name] = $val;
    }


    public function getRequiredProperty($name)
    {
        if (!isset($this->_props[$name])) {
            throw new Exception('No such property: ' . $name);
        }
        return $this->_props[$name];
    }


    public function initializeLogging()
    {
        $level = self::getMonologLoggingLevel();
        $this->_logHandler = new StreamHandler('php://stdout', $level);
        self::$_logger = new Logger('mediaserver');
        self::$_logger->pushHandler($this->_logHandler);
    }


    private static function getMonologLoggingLevel()
    {
        $level = getenv('LOG_LEVEL') ?: 'DEBUG';
        switch ($level) {
            case 'DEBUG':
                return Level::Debug;
            case 'INFO':
                return Level::Info;
            case 'WARNING':
                return Level::Warning;
            case 'ERROR':
                return Level::Error;
        }
        throw new Exception("Invalid logging level: \"$level\". Valid values: DEBUG, INFO, WARNING, ERROR.");
    }
}
