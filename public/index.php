<?php

date_default_timezone_set('Europe/Amsterdam');
set_include_path('.');
define('APPLICATION_PATH', __DIR__);

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\server\MediaServer;

session_start();
if (!isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
}


try {
    $mediaServer = new MediaServer();
    $mediaServer->handleRequest();
} catch (\Exception $e) {
    header('Content-Type:text/plain');
    echo "\n" . $e->getTraceAsString();
    echo "\n" . basename($e->getFile()) . ' (' . $e->getLine() . '): ' . $e->getMessage();
}
